package prgWEB;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/primeiro")
public class Primeiro extends HttpServlet {
	
	private int contadorA;
	private int contadorB;
	
	@Override
	public void init() throws ServletException {
		System.out.println("Servlet Iniciado");
		System.out.println("Contador de Exitos Inicial: " + contadorA);
		System.out.println("Contador de Falhas Inicial: " + contadorB);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        String usuario = req.getParameter("username");
        String senha = req.getParameter("senha");
        
        req.setAttribute("contadorAjsp", contadorA);
        req.setAttribute("contadorBjsp", contadorB);
        
        HttpSession session = req.getSession();
        
        if(usuario.equals("admin") && senha.equals("admin")) {
        	
        	session = req.getSession(true);
        	session.setMaxInactiveInterval(10);
        	
        	contadorA = contadorA + 1;
        	
        	RequestDispatcher dispatcher = req.getRequestDispatcher("PrimeiroJSP.jsp");
    		dispatcher.forward(req, resp);
    		
        }
        
        else {
        	
        	session = req.getSession(false);
        	session.invalidate();
        	
        	contadorB = contadorB + 1;
        	
        	RequestDispatcher dispatcher = req.getRequestDispatcher("ElsePrimeiro.jsp");
    		dispatcher.forward(req, resp);
        	
    		}
		}
	
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		PrintWriter out = resp.getWriter();
		
		String usuario = req.getParameter("username");
        String senha = req.getParameter("senha");
        
        req.setAttribute("contadorAjsp", contadorA);
        req.setAttribute("contadorBjsp", contadorB);
        
        HttpSession session = req.getSession();
        
        if(usuario.equals("admin") && senha.equals("admin")) {
        	
        	session = req.getSession(true);
        	session.setMaxInactiveInterval(10);
        	
        	contadorA = contadorA + 1;
        	
        	RequestDispatcher dispatcher = req.getRequestDispatcher("PrimeiroJSP.jsp");
    		dispatcher.forward(req, resp);
    		
        }
        
        else {
        	
            session = req.getSession(false);
        	session.invalidate();
        	
        	contadorB = contadorB + 1;
        	
        	RequestDispatcher dispatcher = req.getRequestDispatcher("ElsePrimeiro.jsp");
    		dispatcher.forward(req, resp);
        			
    		}
		}
	
	@Override
	public void destroy() {
		System.out.println("Servlet Destru�do");
		System.out.println("Contador de Exitos Final: " + contadorA);
		System.out.println("Contador de Falhas Final: " + contadorB);
	}
		
}


