package prgWEB;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

	@WebServlet ("/rac")
	public class RegistroDeAcesso extends HttpServlet {
		
		private int contadorA;
		private int contadorB;
		
		@Override
		public void init() throws ServletException {
			System.out.println("Servlet Iniciado");
			System.out.println("Contador de Exitos Inicial: " + contadorA);
			System.out.println("Contador de Falhas Inicial: " + contadorB);
		}
		
		@Override
		protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			
			PrintWriter out = resp.getWriter();
			
			String usuario = req.getParameter("username");
	        String senha = req.getParameter("senha");
	        
	        if(usuario.equals("admin") && senha.equals("admin")) {
	        	contadorA = contadorA + 1;
	        }
	        
	        else {
	        	contadorB = contadorB + 1;
	        }
			
			out.println("<html>");
			out.println("<head>");
			out.println("</head>");
			out.println("<body>");
			out.println("<h1>Tentativas que lograram Exito = " + contadorA + "</h1>"); 
			out.println("<h1>Tentativas que falharam = " + contadorB + "</h1>"); 
			out.println("</body>");
			out.println("</html>");
			 
		}
		
		@Override
		public void destroy() {
			System.out.println("Servlet Destru�do");
			System.out.println("Contador de Exitos Final: " + contadorA);
			System.out.println("Contador de Falhas Final: " + contadorB);
		}

	}