package prgWEB;

import java.sql.Connection;  
import java.sql.PreparedStatement; 
import java.sql.SQLException; 
import java.util.Calendar;
import java.util.Date;

public class SQLPessoa implements IPessoa{
	
	Connection con = new ConexaoBD().getConnection();

    public SQLPessoa(){ 
    	
    }
    
    public void Inserir() {
   
           Pessoa pessoa = new Pessoa();    

           String sql =  "insert into cadastro(nome, nascimento, email, idade)VALUES(?,?,?,?)";        

           try {     
               PreparedStatement stmt = con.prepareStatement(sql);
                              
               stmt.setString(1, pessoa.getNome());
               stmt.setDate(2, new java.sql.Date(Calendar.getInstance().getTimeInMillis()));  
               stmt.setEmail(3, pessoa.getEmail());
               stmt.setInt(4, pessoa.getIdade());
               
               stmt.execute();  
               stmt.close();        

           } catch (SQLException u) {        
               throw new RuntimeException(u);        
           }    
           
    }
}
