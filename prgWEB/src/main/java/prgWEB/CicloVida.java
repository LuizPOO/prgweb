package prgWEB;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet ("/ciclovida")
public class CicloVida extends HttpServlet {
	
	private int contador;
	
	@Override
	public void init() throws ServletException {
		System.out.println("Servlet Iniciado");
		System.out.println("Contador Inicial: " + contador);
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		contador = contador + 1;
		
		req.setAttribute("contadorjsp", contador);
		
		RequestDispatcher dispatcher = req.getRequestDispatcher("ResultadoCicloVida.jsp");
		dispatcher.forward(req, resp);
		 
	}
	
	@Override
	public void destroy() {
		System.out.println("Servlet Destru�do");
		System.out.println("Contador Final: " + contador);
	}

}
