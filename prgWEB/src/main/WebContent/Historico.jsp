<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Insert title here</title>
</head>
<body>
  <table border="1">
  
  <tr>
    <th>&nbsp;Indice&nbsp;</th>
    <th>&nbsp;E-mail&nbsp;</th>
    <th>&nbsp;Nome&nbsp;</th>
    <th>&nbsp;Nascimento&nbsp;</th>
    <th>&nbsp;Idade&nbsp;</th>
    <th>&nbsp;Editar&nbsp;</th>
    <th>&nbsp;Excluir&nbsp;</th>
  </tr>
   
  <c:forEach items="${pessoas.values()}" var="p" varStatus="i">
				
  <tr>
	<td><div align="center">${i.count}</div></td>
	<td><div align="center">&nbsp;${p.email}&nbsp;</div></td>
	<td><div align="center">&nbsp;${p.nome}&nbsp;</div></td>
	<td><div align="center"><fmt:formatDate value="${p.nascimento}" pattern="dd-MM-yyyy"/></div></td>
	<td><div align="center">${p.idade}</div></td>
	<td><div align="center"><a href="Editar.jsp?e_mail=${p.email.endereco}"><img src="icon/18editicon.png"/></a></div></td>
	<td><div align="center"><a href="Excluir.jsp?e_mail=${p.email.endereco}"><img src="icon/18deleteicon.png"/></a></div></td>
  </tr>
  
  </c:forEach>
  			
  </table>
</body>
</html>